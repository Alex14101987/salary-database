from fastapi.testclient import TestClient
from app import app, generate_token

client = TestClient(app)

def test_login_page():
    response = client.get("/")
    assert response.status_code == 200
    assert "Login" in response.text

def test_failed_login():
    response = client.post("/get_token", data={"username": "test", "password": "wrong"})
    assert response.status_code == 200
    assert "Invalid credentials" in response.text

def test_successful_login():
    response = client.post("/get_token", data={"username": "bob", "password": "123"})
    assert response.status_code == 200
    assert "token" in response.text

def test_get_salary_with_invalid_token():
    response = client.get("/salary?token=invalid")
    assert response.status_code == 401
    assert "Invalid token" in response.text

def test_generate_token():
    username = "test"
    token = generate_token(username)
    assert token 
