from datetime import datetime


# Пример базы данных с пользователями, зарплатами и датами следующего повышения
users = {
    "bob": {"password": "123", "salary": 50000, "next_raise": datetime(2023, 6, 1)},
    "alice": {"password": "123", "salary": 60000, "next_raise": datetime(2023, 9, 1)}
}