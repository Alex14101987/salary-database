# Используем официальный образ Python в качестве базового
FROM python:3.12

# Устанавливаем рабочую директорию в контейнере
WORKDIR /app

# Копируем файлы проекта в рабочую директорию контейнера
COPY . /app

# Устанавливаем Poetry
RUN pip install --no-cache-dir poetry

# Устанавливаем зависимости через Poetry
RUN poetry install --no-root --no-dev

# Открываем порт для доступа к сервису
EXPOSE 8000

# Команда для запуска сервиса
CMD ["poetry", "run", "uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000"]
