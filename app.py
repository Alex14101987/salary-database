# http://localhost:8000/

from fastapi.staticfiles import StaticFiles
import os
from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from datetime import datetime, timedelta
from database import users

app = FastAPI()

# Получаем абсолютный путь к текущему файлу
current_dir = os.path.dirname(os.path.abspath(__file__))
# Получаем абсолютный путь к папке templates
templates_dir = os.path.join(current_dir, "templates")
# Настройка обработки статических файлов из папки templates
app.mount("/static", StaticFiles(directory=templates_dir), name="static")
templates = Jinja2Templates(directory=templates_dir)

# Пример базы данных с токенами
tokens = {}

# Функция для генерации токена
def generate_token(username: str) -> str:
    expiration_time = datetime.now() + timedelta(seconds=10)
    token = str(hash(username + str(expiration_time)))
    tokens[token] = {"username": username, "expiration_time": expiration_time}
    return token

# Функция для проверки токена
def check_token(token: str) -> str | None:
    token_data = tokens.get(token)
    if token_data and token_data["expiration_time"] > datetime.now():
        return token_data["username"]
    else:
        return None

# Маршрут для отображения страницы ввода логина и пароля
@app.get("/", response_class=HTMLResponse)
async def login(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})

# Маршрут для аутентификации и выдачи токена
@app.post("/get_token")
async def get_token(request: Request, username: str = Form(...), password: str = Form(...)):
    if username in users and users[username]["password"] == password:
        token = generate_token(username)
        return templates.TemplateResponse("token.html", {"request": request, "token": token, "tokens": tokens})
    else:
        return templates.TemplateResponse("failed_login.html", {"request": request, "error_message": "Invalid credentials"})

@app.get("/salary", response_class=HTMLResponse)
async def get_salary(request: Request, token: str):
    username = check_token(token)
    if username is None:
        return templates.TemplateResponse("failed_token.html", {"request": request, "error_message": "Invalid token"}, status_code=401)
    try:
        salary = users[username]["salary"]
        next_raise = users[username]["next_raise"]
    except KeyError:
        return templates.TemplateResponse("failed_token.html", {"request": request, "error_message": "User not found"}, status_code=404)
    return templates.TemplateResponse("salary.html", {"request": request, "username": username, "salary": salary, "next_raise": next_raise})

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

